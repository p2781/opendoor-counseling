>>>
Days November 9-16, 2021

This is markdown, a easier way to make HTML files.
Any questions asked will be given.

Copyrighted to Bradley Howell II, Year 2021

Some of the text is copied from this website: https://www.lifecoachcode.com/2018/06/19/mind-games-narcissists-play-how-to-use-them/
>>>

# 7 narcisism things I notice and what they mean

##### 1. They want to know the details of your life.

Manipulators don’t talk you about themselves. Instead, they’ll pretend they’re interested in you and ask even the tiniest detail of yourself, especially your thoughts and plans.

Without your knowledge, they will use this information against you to their own advantage.

>I do notice this in my behavior, as I do try to gather information. Sometimes I wonder about someone's history in the military b/c I like the military and what they do for our country.

##### 2. They want to send you a subtle message of superiority.

In their quest to appear superior, manipulators will provide you with massive information of figures, stats and numbers on things you’re not familiar with.

They want to show off how smart they are with the hidden intention of exerting their influence over you.

>I do notice this in my behavior, but to mention it's not working for me and I keep on trying. (Repetiveness)

##### 3. They overwhelm you with negativity.

At times, manipulators intentionally act negatively, like speaking in a loud voice or showing ill manners. This is their way of controlling the situation and manipulating other people.

They know that fear is a powerful tool to make people listen, back off and control them. So they will use it in a form of aggression or painting negative picture to lower the energy. High energy cannot be easily controlled so they try to lower it.

>I do notice this in my behavior, but something inside me refrains me from doing it as it creates fear inside myself as well.

##### 4. They feed on your insecurity.

If you’re struggling with some insecurity issues, manipulators are able to smell that. In fact, they love feeding on the insecurities of other people.

If you show and reveal some insecurity to them, they will use it to climb higher than you on the social hierarchy. They will use your insecurity in conversations. It makes them feel superior and good about themselves. Other people’s insecurities mask their own.

>I do notice this in my behavior, but also it's quite hard to disingush whether or not.

##### 5. They’ll act innocent.

As what we have stated above, narcissists are con artists who are really good at manipulation. They will convince you of their innocence whenever they mess up.

They may appear blameless from the outside, but inside they’re the sly fox who keeps thinking on more ways to appear innocent.

> I do remember this technique, it is known as the vitim stance sexual harrasers do for control over the other person. Pain though to admit, I do use this technique often.

##### 6. They twist information.

When the misdeeds of manipulators are revealed, they will twist the information and create stories where they can appear as the victim. Finding other people to blame is their means of escape.

If you’re not too careful, they may manipulate you into believing their stories. Don’t allow manipulators to get away with it.

>This, I really notice. I noticed b/c of my enviorment around me and what's happening.

##### 7. They put pressure on you.

Manipulators will drive you crazy if you allow them to. They have this ability to put pressure on your shoulders until you no longer can contain it and erupt into an emotional outburst.

This is what psychologists call the technique of fear and relief, which is a manipulator’s way of preying on your emotions.

>I put pressure indeedly on my mom in order to find out the reactions she gives, although I know it gets me in trouble, I observe the enviornment around me, and I am the only person in the family that snapped into what's happening.
